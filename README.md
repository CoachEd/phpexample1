# Basic PHP Login

This is a simple example of a PHP login page. The username and password are hard-coded. A real solution would store these values in a database.

## Getting Started

1. Clone this repository.
1. Drop the contents of this repository in the root folder of your web application.
1. Point your browser to the index.php file.
1. To log in, user/password are *user/password*.


