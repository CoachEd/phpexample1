<?php include 'header.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>intensity</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php include 'menubar.php';?>

<h2>Welcome to the intensity portal.</h2>

<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div id="content">
						<p id="msg"></p><br/>
			            <br/>
			            <br/>
			            <br/>
						<br/>
						<br/>
				</div>
            </div>
        </div>
    </div>
</div>


<script>
<?php
if (isset($_SESSION["msg"])) {
    echo "document.getElementById('msg').innerHTML='".$_SESSION["msg"]."';";
}else{
    // Fallback behaviour goes here
    echo "document.getElementById('msg').innerHTML='';";
}
?>
</script>

</body>
<?php
unset($_SESSION["msg"]); //do last
?>
</html>
