<?php
session_start();
$loggedin = $_SESSION["loggedin"]; //username
if (strlen($loggedin) === 0) {
	header('Location: index.php' ) ;
}
//get user profile image URL
$imageUrl = "./img/".$loggedin.".png";
$url=getimagesize($imageUrl);
if(!is_array($url)) {
	// The image does not exist
	$imageUrl = "./img/default.png";
}
?>
